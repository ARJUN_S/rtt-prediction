''' Desc: Calcuates number of lost packets and prints PERCENT lost packets by connection.
    Params: argv[1:] = .pcap files
    Author: Arjun Subramonian   '''

import pyshark
from sys import argv
import netaddr

target = 2154969872
cons = {}
packets = {}
for filename in argv[1:]:
    pcap = pyshark.FileCapture(filename)
    for pkt in pcap:
        if pkt.transport_layer is None:
            continue
        con = str(netaddr.IPAddress(pkt.ip.src)) + ' > ' + str(netaddr.IPAddress(pkt.ip.dst))
        seq = pkt.tcp.seq
        end = int(seq) + int(pkt.tcp.len)
        # print '!', seq, end
        if con not in cons:
            cons[con] = end
            packets[con] = (0.0, 1)
            continue
        if end >= cons[con]:
            cons[con] = end
        else:
            packets[con] = (packets[con][0] + 1, packets[con][1])
            # print end, cons[con]
        packets[con] = (packets[con][0], packets[con][1] + 1)
    # for con in packets:
        # print con + ': ' + str(packets[con][0] / packets[con][1])

data = {}
count = 0
first = ''
with open('table.txt', 'r') as f:
    for line in f:
        if count == 0:
            first = line
            count += 1
            continue
        arr = line.strip().split();
        if arr[0] in packets:
            data[arr[0]] = [packets[arr[0]]] + arr[1:]
        count += 1
with open ('table.txt', 'w') as f:
    for key in data:
        f.write('{:20s}'.format(key))
        [f.write('{:20s}'.format(elem)) for elem in data[key]]
        f.write('\n')