''' Desc: Given a training file (for Jitter), a test file, and start and end line values, prints time sent, RTT, and RTO values in table format.
    Params: argv[1] = training file, argv[2] = test file, argv[3] = start line val, argv[4] = end line val (utilized by bash file)
    Author: Arjun Subramonian   '''

from machine_learning import MachineLearning
from model_based import ModelBased
from jitter import JitterPredictor
from jacobson import Jacobson
from weightedmedians import RWM
from Mep import MEP

from sys import argv
import fileinput
import datetime

if __name__ == "__main__":
    predictors = [
        MachineLearning(),
        ModelBased(),  
        JitterPredictor(open(argv[1], "r")),
        RWM(), 
        Jacobson(),
        MEP(),
    ]        
    
    count = 0
    start = False
    for line in fileinput.input(argv[2]):
        if count == int(argv[3]):
            start = True
        elif count == int(argv[4]):
            break
        if start == False:
            count += 1
            continue
        arr = line.strip().split()
        rtt = float(arr[0])
        time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
        print '{:20s}'.format(str(arr[-1])),
        print '{:20s}'.format(str(rtt)),
        for p in predictors:
            p.receive_rtt(rtt, time)
            if not isinstance(p.rto, float) and not isinstance(p.rto, int):
                p.rto = p.rto.item(0)
            print '{:20s}'.format(str(p.rto)),
        print
        count += 1