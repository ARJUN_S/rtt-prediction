''' Desc: Creates database for notes on graphs.
    Author: Daniel Alves    '''

import sqlalchemy as sql
from sqlalchemy import Column, Integer, String, Unicode, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from fileinput import input
from db import City, Country, Market, Note, Session, Base, engine

def get_market(country_code):
    codes = {
        "au": "OC",
        "be": "EU",
        "br": "SA",
        "ca": "NA",
        "ch": "EU",
        "cn": "AS",
        "de": "EU",
        "dk": "EU",
        "es": "EU",
        "fi": "EU",
        "fr": "EU",
        "gr": "EU",
        "hk": "AS",
        "ie": "EU",
        "il": "AS",
        "in": "AS",
        "it": "EU",
        "jp": "AS",
        "kr": "AS",
        "nz": "OC",
        "ph": "AS",
        "nl": "EU",
        "no": "EU",
        "nz": "OC",
        "ph": "AS",
        "pl": "EU",
        "ru": "EU",
        "se": "EU",
        "sn": "AF",
        "tw": "AS",
        "uk": "EU",
        "us": "NA",
        }
    if country_code in codes:
        return codes[country_code]
    return "XX"

# Give files with data as argument
if __name__ == "__main__":
    session = Session()
    Base.metadata.create_all(engine)
    counter = 0
    cities = {}
    countries = {}
    markets = {}
    for line in input():
        line = line.strip()
        if line:
            try:
                (year, month, day, city, country, note) = line.split(';', 5)
            except ValueError:
                print line
                exit()
            city = city.lower()
            country = country.lower()
            year = int(year)
            month = int(month)
            day = int(day)
            if city not in cities:
                if country not in countries:
                    market = get_market(country)
                    if market not in markets:
                        markets[market] = Market(name=market)
                    countries[country] = Country(name=country,
                                                 market=markets[market])
                cities[city] = City(name=city, country=countries[country])
            new_note = Note(year=year, month=month, day=day,
                            city=cities[city], note=unicode(note, "utf-8"))
            session.add(new_note)
            #counter += 1
            #if counter % 10000 == 0:
                #session.commit()
                ## TODO: change the numbers
                #print "%f%%" % (counter/42766589.0*100)
    session.commit()
    
