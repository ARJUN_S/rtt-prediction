#!/bin/bash

''' Desc:
    Author: Daniel Alves    '''

for file in $@
do
    echo $file
    tcpdump -r $file -tt | python parse.py
done

