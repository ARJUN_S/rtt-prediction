from client import Client

if __name__ == '__main__':
    clients = [
        Client()
    ]

    for c in clients:
        c.run_program()
