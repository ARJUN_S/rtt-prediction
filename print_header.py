''' Desc: Prints header for table-formatted files.
    Params: argv[1] = training file for Jitter (utilized by bash file)
    Author: Arjun Subramonian   '''

from machine_learning import MachineLearning
from model_based import ModelBased
from jitter import JitterPredictor
from jacobson import Jacobson
from weightedmedians import RWM
from Mep import MEP 

from sys import argv

predictors = [
    MachineLearning(), 
    ModelBased(), 
    JitterPredictor(open(argv[1], "r")), 
    RWM(),
    MEP(),
    Jacobson(), 
]

print '{:20s}'.format('Connection'),
print '{:20s}'.format('ActualLoss'),
for p in predictors:
    print '{:20s}'.format(type(p).__name__),
print