''' Desc: Checks to see if any note in database didn't match given categories.
    Author: Daniel Alves (Modified: Arjun Subramonian)  '''

from db import Session, Market, Country, City, Note
from sqlalchemy import func

session = Session()

for market_id, market in session.query(Market.id, Market.name):
    query = session.query(Note.note, Note.year, Note.month, Note.day,
                          City.name, Country.name).join(City).join(Country).\
                          join(Market).filter(Country.market_id == market_id)
    for note, year, month, day, city, country in query.order_by(Country.name, Market.name):
        print "%d-%d-%d-%s-%s-%s:" % (year, month, day, market, city, country),
        print note
        
