import numpy as np
import math

class Client:
    def __init__(self):
        self.RTO = 1000
        self.time = 0
        self.packet_size = 40
        self.req = False
        self.resp = False
        self.MTU_m = 1500
        self.MTU_e = 576
        self.queue = []

    def run_program(self):
        # Steps 1 + 2 + 3
        for i in range(0, 2):
            self.wait_tc()

        # Step 4
        S = 0
        while S < 50 or S > 1048576 * 2:
            S = np.random.lognormal(8.35, 1.37)
        CWD = 1
        CWD = self.sub_program(S, CWD, True)

        # Step 6
        Tp = np.random.exponential(0.13)
        self.time += Tp
        print Tp, self.time

        # Step 7
        Nd = 54
        while (Nd > 53):
            Nd = int(np.random.pareto(1.1) * 55 - 2)
        if (Nd > 0):
            for i in range(0, 4):
                self.wait_tc()
            for i in range(1, Nd):
                S = 0
                while S < 50 or S > 1048576 * 2:
                    S = np.random.lognormal(8.35, 1.37)
                CWD = self.sub_program(S, CWD, False)
        else:
            self.wait_tc()
            for i in range(0, 2):
                self.wait_tc()

    def wait_tc(self):
        Tc = np.random.exponential(50)
        self.time += Tc
        print Tc, self.time

    def sub_program(self, S, W, main):
        # Step 1
        N = 0
        if main:
            N = math.ceil(S / (self.MTU_m - 40))
        else:
            N = math.ceil(S / (self.MTU_e - 40))

        # Step 2
        C = min(N, W)

        # Step 3
        P = N - C

        # Step 4
        while P > 0:
            self.wait_tc()
            P -= min(P, 2)
        PW = N + W
        return PW


        


