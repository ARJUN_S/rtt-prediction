''' Desc: APPENDIX D: FORMULA TO DEFINE VARIOUS THROUGHPUT AND DELAY DEFINITIONS
    *** Utility class ***
    Author: Arjun Subramonian '''

import numpy as np

class AppD:

    # M = total number of independent runs (for a specific configuration).
    # N = total number of data users in each run (for a sector).

    # m = index of the simulation runs, i.e., m = 1,2,3, ..., M.
    # n = index of a data user within a simulation run, i.e., n = 1,2,..., N.

    def __init__(self, T, M, N):
        self.T = T
        self.M = M
        self.N = N

        self.user = np.zeros([M, N])
        # K(m,n) = total number of packet calls generated for user(m,n).
        self.K = np.zeros(self.user.shape)

        # k = index of packet calls for a user. For user(m,n),, k = 1, 2, ..., K(m,n).
        # L(m,n,k) = total number of packets generated for the k-th packet call of user(m,n).
        self.L = np.zeros([M, N, self.K[M][N]])

        # l = index of packet within a packet call. For the k-th packet call of user(m,n), l = 1,2, ..., L(m,n,k).
        # B(m,n,k,l) = number of information bits contained in the l-th packet of the k-th packet calls for user(m,n) . If the packet is not successfully delivered by the end of the simulation run, B(m,n,k,l) = 0.
        self.B = np.zeros([M, N, self.K[M][N], self.L[M][N][self.K[M][N]]])

        # TA(m,n,k,l) = arrival time of the l-th packet of the k-th packet calls for user(m,n). it is the time when the packet arrives at the transmitter side and is put into a queue.
        self.TA = np.zeros(self.B.shape)
        # TD(m,n,k,l) = delivered time of the l-th packet of the k-th packet calls for user(m,n) . It is the time when the receiver successfully receives the packet. Due to fixed simulation time, there may be packets waiting to be completed at the end of a simulation run. For these packets, the delivered time is the end of the simulation.
        self.TD = np.zeros(self.B.shape)

        # PCTA(m,n,k) = arrival time of the k-th packet call for user(m,n), it is the time when the first packet of the packet call arrives at the transmitter side and is put into a queue.
        self.PCTA = np.zeros(self.L.shape)
        # PCTD(m,n,k) = delivered time of the k-th packet call for user(m,n). It is the time when the receiver successfully receives the last packet of the packet call. Due to fixed simulation time, there may be packet calls waiting to be completed at the end of a simulation run. For these packet calls, the delivered time is the end of the simulation.
        self.PCTD = np.zeros(self.L.shape)

       # PCTA(m,n,k) = TA(m,n,k,1) and PCTD(m,n,k) = TA(m,n,k,L(m,n,k))

    def data_thruput_ps(self):
        res = 0
        for m in range(0, self.M):
            for n in range(0, self.N):
               res += data_thruput_fu(m, n)
        return res / self.M * self.T

    def aved_delay_ps(self):
        res1 = 0
        res2 = 0
        for m in range(0, self.M):
            for n in range(0, self.N):
                res2 += self.K[m][n]
                for k in range(0, self.K[m][n]):
                    res1 += self.PCTD[m][n][k] - self.PCTA[m][n][k]
        return res1 / res2

    def data_thruput_fu(self, m, n):
        res = 0
        for k in range(0, self.K[m][n]):
            for l in range(0, self.L[m][n][k]):
                res += self.B[m][n][k][l]
        return res / self.T

    def packet_call_thruput_fu(self, m, n):
        res1 = self.data_thruput_fu(self, m, n) * T
        res2 = 0
        for k in range(0, self.K[m][n]):
            res2 += self.PCTD[m][n][k] - self.PCTA[m][n][k]
        return res1 / res2

    def aved_packet_delay_ps(self):
        res1 = 0
        res2 = 0
        for m in range(0, self.M):
            for n in range(0, self.N):
                for k in range(0, self.K[m][n]):
                    res2 += self.L[m][n][k]
                    for l in range(0, self.L[m][n][k]):
                        res1 += self.TD[m][n][k][l] - self.TA[m][n][k][l]
        return res1 / res2

    def aved_packet_delay_fu(self, m, n):
        res1 = 0
        res2 = 0
        for k in range(0, self.K[m][n]):
            res2 += self.L[m][n][k]
            for l in range(0, self.L[m][n][k]):
                res1 += self.TD[m][n][k][l] - self.TA[m][n][k][l]
        return res1 / res2

    def aved_packet_call_delay_fu(self, m, n):
        res = 0
        for k in range(0, self.K[m][n]):
            res += self.PCTD[m][n][k] - self.PCTA[m][n][k]
        return res / self.K[m][n]



