''' Desc: Prints all valid connections in the form src, dst, ack, beg seq val, end seq val, and dt.
    Params: argv[1:] = .pcap files
    Author: Daniel Alves    '''

import pyshark
from sys import argv
import netaddr

target = 2154969872
for filename in argv[1:]:
    pcap = pyshark.FileCapture(filename)
    for pkt in pcap:
        if pkt.transport_layer is None:
            continue
        src = int(netaddr.IPAddress(pkt.ip.src))
        dst = int(netaddr.IPAddress(pkt.ip.dst))
        try:
            ack = pkt.tcp.ack
        except(AttributeError):
            ack = 0
        seq = pkt.tcp.seq
        len = pkt.tcp.len
        dt  = pkt.sniff_time
        if dst == target or src == target:
            print src, dst, ack, seq, int(seq)+int(len), dt
