''' Desc: Separates tcp_exchange.dat into different .txt files based on connection given in a line.
    Params: tcp_exchange.dat
    Author: Daniel Alves    '''

from fileinput import input

for line in input():
    dst, src, rtt, dt = line.strip().split()
    filename = "%s-%s-rtt.dat" % (dst, src)
    with open(filename, "a") as f:
        f.write(" ".join([rtt, dt]) + "\n")
