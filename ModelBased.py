A model based rtt prediction algorithmfrom rttpredictor import RTTPredicto
from fileinput import input
import datetime
import numpy as np 
# Model Based Algorithm for RTT Predictor, uses recursion and predicts future rtts
# Ishani Karmarkar

class ModelBased(RTTPredictor):
    def __init__(self, under_error=1, over_error=-1, lambda = .97):
        """Initializes the Jacobson algorithm (RFC6298)."""
        RTTPredictor.__init__(self, under_error, over_error)
        self.srtt = None
        self.rttvar = None
        self.history = [ ]
        self.k = 0
        self.a = None
        self.b = None
        self.weight = lambda 
        for line in input: 
            arr = line.strip().split()
            yk = float(arr[0])
            sk = datetime.datetime.strptime(currline[-2] + "" + currline[-1], "%Y-%m-%d %H:%M:%S:%f")
            self.history.append(yk, sk)
            self.k += 1

    def u(self, k): 
        if k == 0: 
            return 0 
        return self.history[k][1] - self.history[k -1][1]

    def deltay(self, k): 
        if k == 0: 
            return 0
        return self.history[k][0] - self.history[k-1][1]

    def deltau(self, k): 
        return self.u(k) - self.u(k -1)

    def phi(self, k): 
        phi = [ ]
        counter = 1
        while counter < k: 
            phi.append(-self.deltay(k - counter))
            counter += 1
        timer = 1
        while timer < k: 
            phi.append(self.deltau(k - timer))
            timer += 1
        phiarr = np.asarray(phi)
        transposedphiarr = phiarr[np.newaxis].T
        return [phiarr, transposedphiarr]  

    def theta(self, k): 
    	if k == 1: 
    		return 1 + self.L(k) * (self.deltay(k) - phiarr)
    	phiarr = np.asarray(phi)[0]
    	theta = self.theta(k-1) + self.L(k) * (self.deltay(k) - phiarr.dot(self.theta(k-1)))
        lists = np.ravel(theta)
        thetaAs = lists[0: len(lists)/2]
        thetaBs = lists[len(lists)/2:]
        thetaArr.append(theta)
        thetaArr.append(thetaAs)
    	thetaArr.append(thetaBs)
        return thetaArr 

    def P(self, k):  
        Pk = [ ]
        if k == 0: 
            return 1  
        if k == 1: 
            phiarr = self.phi(k)[0]
            transposedphiarr = self.phi(k)[1]
            itemNumerator =(1 - phiarr.dot(transposedphiarr))
            itemDenominator = (self.weight + phiarr.dot(transposedphiarr)
            return (1/self.weight)(itermNumerator/itemDenominator)
        
        Pk = (1/self.weight)(itermNumerator/itemDenominator)
        phiarr = self.phi(k)[0]
        transposedphiarr = self.phi(k)[1]
        itemNumerator =(self.P(k-1) - self.P(k-1).dot(phiarr).dot(transposedphiarr).dot(self.P(k-1))
        itemDenominator = (self.weight + phiarr.dot(self.P(k-1)).dot(transposedphiarr)
        Pk = (1/self.weight)(itermNumerator/itemDenominator)
        return Pk

    def L(self, k): 
        phiarr = self.phi(k)[0]
        transposedphiarr = self.phi(k)[1]
        if k == 1: 
            ((transposedphiarr))/(self.weight + phiarr.dot(transposedphiarr)
        Lk = (self.P(k-1).dot(transposedphiarr))/(self.weight + phiarr.dot(self.P(k-1)).dot(transposedphiarr)
        return Lk 

    def update_rto(self, rtt, count):

        thetaArr = theta(count)
        As = thetaArr[1]
        Bs = thetaArr[2]
        diff = 0 

        for k in As: 
            diff -= (As[k] * deltay(k))
            diff += (Bs[k] * deltay(k))

        self.rto = y(k) + diff

    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)
        self.rto = min(2*self.rto, 60*1000)


