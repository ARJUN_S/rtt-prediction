#!/usr/bin/env python
# coding: utf-8

''' Desc:
    Author: Daniel Alves    '''

from re import compile
from fileinput import input, filename

ip = r'\d+(?:\.\d+){3}'
header = compile(r'(\d+\.\d+) IP (%s)\.\S+ > (%s)\.\S+: Flags \[(.*?)\],(.*)' %
                 (ip, ip))
extra_pattern = compile(r'(\w+) (\S+|\[.*\])(?:,|$)')

flags_dict = {
    'S': 'syn',
    '.': 'ack',
    'P': 'push',
    'R': 'reset',
    'F': 'fin',
    }

def process_line(line):
    l = line.strip()
    result = header.match(l)
    if not result:
        raise ValueError
    time, src, dst, flags_bits, other = result.groups()
    time = float(time)
    flags = set()
    for f in flags_bits:
        if f in flags_dict:
            flags.add(flags_dict[f])
        else:
            raise ValueError
    extra = {}
    for key, element in extra_pattern.findall(other):
        if element.startswith('['):
            extra[key] = {}
            for node in element[1:-1].split(','):
                node = node.strip()
                if node.startswith('eol'):
                    extra[key]['eol'] = 1
                elif node.startswith('TS'):
                    blocks = node.split()[1:]
                    extra[key]['TS'] = {}
                    for i in xrange(len(blocks)/2):
                        extra[key]['TS'][blocks[i*2]] = int(blocks[i*2+1])
        else:
            try:
                element = int(element)
            except ValueError:
                try:
                    element = float(element)
                except ValueError:
                    pass
            extra[key] = element
    return (time, (src, dst), flags, extra)

if __name__ == "__main__":
    for line in input():
        (time, addresses, flags, extra) = process_line(line)
        print time, addresses, flags, extra



