''' Desc: "Modifies" duplicates.txt so that all output files only contain duplicates for one connection, and solely the RTT and time sent values.
    Author: Arjun Subramonian   '''

# with open('duplicates.txt') as f:
#     key = ''
#     count = 1
#     output = ''
#     for line in f:
#         line = line.strip()
#         if len(line) == 0 or key == line:
#             count += 1
#             continue
#         if count == 1 or ('.dat' in line and key != line):
#             key = line
#             output += key + '\n\n'
#         else:
#             output += line + '\n'
#         count += 1
# print output

with open('duplicates.txt') as f:
    key = ''
    output = ''
    for line in f:
        line = line.strip()
        if len(line) == 0:
            continue
        if '.dat' in line:
            key = line.split('/')[1].split('.')[0] + '.txt'
        else:
            with open(key, 'a') as file:
                file.write(' '.join(line.split()[2:]) + "\n")