#!/usr/bin/python

''' Desc: Runs through all predictors, and calculates number of lost packets and error using actual RTTs and their predicted values.
    Params: argv[1] = training file for Jitter, argv[2:] = test files
    Author: Daniel Alves (Modified: Arjun Subramonian)  '''

from machine_learning import MachineLearning
from model_based import ModelBased
from jitter import JitterPredictor
from jacobson import Jacobson
from weightedmedians import RWM
from Mep import MEP 

from sys import argv
import fileinput
import datetime

if __name__ == "__main__":
    predictors = [
        MachineLearning(), 
        ModelBased(), 
        JitterPredictor(open(argv[1], "r")), 
        RWM(),
        MEP(),
        Jacobson(), 
    ]

    count = 0
    for line in fileinput.input(argv[2:]):
        arr = line.strip().split()
        rtt = float(arr[0])
        time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
        for p in predictors:
            p.receive_rtt(rtt, time)
        count += 1

    for p in predictors:
        print type(p).__name__, p.lost_packets*100.0/count,
        print p.error
    
